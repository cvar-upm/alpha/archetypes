
#include <cmath>
#include "rclcpp/rclcpp.hpp"
#include "arty_example_sinusoid_emitter/trigger_gen.hpp"

namespace arty_example_sinusoid_emitter
{

/**
 * Global variables:
 */

rclcpp::Logger trigger_logger = rclcpp::get_logger("trigger_logger");

bool only_positive = false;

/**
 * Generate a sinusoid
 */
void tick_triggered(const inputs & in, outputs & out, responses &)
{
  RCLCPP_DEBUG_STREAM(trigger_logger, "Executing tick");

  auto now = rclcpp::Clock().now();
  double t = now.seconds();

  // Computes the sinusoid
  out.sin.data = in.amplitude * std::sin(2.0 * M_PI * in.frequency * t) + in.offset.data;
  // Publishs result

  if (only_positive && out.sin.data < 0) out.sin.data = 0;

  out.sin.publish();

  RCLCPP_DEBUG_STREAM(trigger_logger, "Done: tick");
}

void only_positive_triggered(
  const inputs &,
  outputs &,
  responses &,
  const std::shared_ptr<std_srvs::srv::SetBool::Request> request,
  std::shared_ptr<std_srvs::srv::SetBool::Response> response)
{
  RCLCPP_INFO_STREAM(trigger_logger, "only_positive_triggered");
  if (only_positive == request->data) {
    response->success = false;
    response->message = "Already set/unset";
  } else {
    only_positive     = request->data;
    response->success = true;
    response->message = "OK";
  }
  RCLCPP_INFO_STREAM(trigger_logger, "Done: only_positive_triggered");
}

}  // namespace arty_example_sinusoid_emitter