
#include "arty_example_alternate_positive/trigger_gen.hpp"

namespace arty_example_alternate_positive
{

rclcpp::Logger trigger_logger = rclcpp::get_logger("trigger_logger");

void tick_triggered(const inputs &, outputs & out, responses & recv)
{
  RCLCPP_DEBUG_STREAM(trigger_logger, "Executing tick");
  static bool first_request = true;

  if (first_request) {
    // Send first request
    RCLCPP_INFO_STREAM(trigger_logger, "Send a new request");
    out.only_positive->data = false;
    out.only_positive.async_request();
    first_request = false;
  } else if (recv.only_positive) {
    RCLCPP_INFO_STREAM(trigger_logger, "Processing received request");
    if (!recv.only_positive->success) {
      RCLCPP_INFO_STREAM(trigger_logger, "Received false result");
    }
    RCLCPP_INFO_STREAM(trigger_logger, "Sending another request");
    out.only_positive->data = !out.only_positive->data;
    out.only_positive.async_request();
    recv.only_positive.reset();
  } else {
    RCLCPP_INFO_STREAM(trigger_logger, "Awaiting response");
  }

  RCLCPP_INFO_STREAM_ONCE(trigger_logger, "Done: tick");
}

}  // namespace arty_example_alternate_positive