cmake_minimum_required(VERSION 3.8)
project(arty_plugin)

find_package(ament_cmake REQUIRED)

# Install template
install(DIRECTORY template/
    DESTINATION share/${PROJECT_NAME}/template
)

# Install archetype metadata
install(DIRECTORY metadata/
    DESTINATION share/${PROJECT_NAME}/metadata
)

# Install archetype macros
install(DIRECTORY common/
    DESTINATION share/${PROJECT_NAME}/common
)

ament_package()
