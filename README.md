# archetypes

A tool for automated code generation of ROS2 nodes.

## Install

Download, build, and source the packages in the repository, they are:

- arty_generator (the ros2 command to generate the code)
- arty_basic_node (a template of a simple node)
- arty_plugin_manager (a template of the node that manages plugins and include a default plugin)
- arty_plug (a template of a plugin to work with the previous template)

Try:

```bash
  ros2 generate --help
```

to ensure the command in `arty_generator` is correctly installed.

Remember to source your ROS 2 installation and your workspace:

```bash
source /opt/ros/humble/setup.bash  # (replace humble with your version)
source install/setup.bash
ros2 --help
```

## Getting started

- Change to your working dir
- Write your manifest file
- Generate the code using:
  ```bash
    ros2 generate arty_basic_node
  ```
  where `arty_basic_node` is the template name.

Find in the `examples` folder, some manifest.yaml to understand what information is needed and the expected structure of the manifest file.
