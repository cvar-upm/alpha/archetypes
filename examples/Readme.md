# Examples

This folder presents some examples that doesn't include the generated files.

They are provided to be ready to execute the `generate` command.

The file COLCON_IGNORE is included to avoid building these packages. Just remove or rename it to build and test them.