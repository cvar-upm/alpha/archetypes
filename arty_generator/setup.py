from setuptools import find_packages, setup

package_name = 'arty_generator'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='stapia',
    maintainer_email='santiago.tapia@upm.es',
    description='TODO: Package description',
    license='Apache-2.0',
    tests_require=['pytest'],
    entry_points={
        # 'console_scripts': [
        # ],
        'ros2cli.command': [
            'generate = arty_generator.arty_generator:MainClass',
        ],
    },
)
