import argparse
import os
import sys
import re
import yaml
from jinja2 import Environment, FileSystemLoader
from ament_index_python.packages import get_package_share_directory
from datetime import datetime
from ros2cli.command import CommandExtension

# Custom filters
def unique_types(publishers):
    """Return unique 'type' in publisher list """
    return list(set(pub["type"] for pub in publishers if "type" in pub))

# Function helpers
def check_subfolder_exists(share_path, subfolder, required = True):
    """
    Checks if a subfolder exists within the 'share' directory.

    :param share_path: Base path where 'share' is located.
    :param subfolder: Name of the subfolder to check.
    :return: Full path if it exists, otherwise None.
    """
    full_path = os.path.join(share_path, subfolder)

    if not os.path.isdir(full_path) and required:
        sys.exit(f"Error: The subfolder '{subfolder}' does not exist in '{share_path}'.")

    if not os.path.isdir(full_path):
        return None

    return full_path

def process_depend_data(metadata, args):
    result = list()
    dependencies = metadata['archetype'].get('depends')
    for pkg in dependencies or []:
        try:
            if args.verbose: print(f"Looking for dependencies in package {pkg}")
            shared_path = get_package_share_directory(pkg)

            if args.verbose: print(f"Looking for common package subfolder")
            common_folder = check_subfolder_exists(shared_path, 'common', required=False)

            if common_folder is not None and args.verbose: print(f"\tCommon subfolder found at: {common_folder}")

            if common_folder is not None:
                result.append(common_folder)
        except:
            print(f"Error: error looking for dependencies, package was: {pkg}")

    return result

def display_tree(directory, indent=""):
    """
    Recursively displays the contents of a directory in a tree-like structure.

    :param directory: The path of the directory to display.
    :param indent: Indentation string for nested files/folders (used internally).
    """
    if directory is None: return

    items = sorted(os.listdir(directory))
    for index, item in enumerate(items):
        item_path = os.path.join(directory, item)
        is_last = index == len(items) - 1  # Check if it's the last item

        # Use ├── for items except the last, └── for the last one
        prefix = "└── " if is_last else "├── "
        print(indent + prefix + item)

        # If the item is a directory, recurse with deeper indentation
        if os.path.isdir(item_path):
            new_indent = indent + ("    " if is_last else "│   ")  # Adjust indentation
            display_tree(item_path, new_indent)

class MainClass(CommandExtension):
    """Archetype command for automatic code generation"""

    def add_arguments(self, parser: argparse.ArgumentParser, cli_name: str) -> None:
        parser.add_argument('package', type=str, nargs='?', help='template package')
        parser.add_argument('--verbose', action='store_true', help='Active detailed information')
        parser.add_argument('--tree', action='store_true', help='Show files in template folder and exits')
        parser.add_argument('--metadata', action='store_true', help='Show package metada')

        # parser.add_argument('datos', type=str, nargs='?', default="Nadie", help='Sin --')
        # parser.add_argument('--nombre', type=str, help='Nombre a mostrar')

    def main(self, parser: argparse.ArgumentParser, *, args) -> int:
        try:
            result = self.main_inner(parser, args=args)
            return result
        except Exception as e:
            print("An exception was found at generating the code")
            print(f"Exception was: {e}")
        return 1

    def main_inner(self, parser: argparse.ArgumentParser, *, args) -> int:
        # nombre = args.nombre if args.nombre else "Mundo"
        # print(f"¡Hola, {nombre}! con los datos: {args.datos}")
        print("Archetype automatic code generation")

        if args.package: print(f"Using package: `{args.package}'")
        else:
            print("Error: template package is required")
            return 1

        shared_path = get_package_share_directory(args.package)
        if args.verbose: print(f"Looking for package subfolders")

        metadata_folder = check_subfolder_exists(shared_path, 'metadata')
        if args.verbose: print(f"\tMetadata subfolder found at: {metadata_folder}")

        common_folder = check_subfolder_exists(shared_path, 'common', required=False)
        if common_folder is not None and args.verbose: print(f"\tCommon subfolder found at: {common_folder}")

        template_folder = check_subfolder_exists(shared_path, 'template')
        if args.verbose: print(f"\tTemplate subfolder found at: {template_folder}")

        if args.tree:
            print("=== Common jinja templates (will not be rendered directly) === ")
            display_tree(common_folder)
            print("=== Arquetype templates (will be rendered directly using the same tree) ===")
            display_tree(template_folder)
            return 0

        # Load template metadata:
        if args.verbose: print("Reading template package information")
        metadata_yaml = os.path.join(metadata_folder, 'metadata.yaml')
        with open(metadata_yaml, "r") as f:
            try:
                metadata = yaml.safe_load(f)
            except yaml.YAMLError as e:
                raise ValueError(f"Error while parsing {metadata_yaml} file: {e}")

        if args.verbose or args.metadata:
            print(f"=== Archetype metadata ===\n{yaml.dump(metadata)}")

        if args.verbose: print("   Done: Reading template package information")

        if args.verbose: print("Processing depend data")
        other_common_dirs = process_depend_data(metadata, args)

        if args.verbose: print("   Done: Processing depend data")

        # Load manifest file:
        if args.verbose: print("Reading manifest file")
        wd = os.path.abspath(".")
        manifest_path = os.path.join(wd, 'manifest.yaml')
        with open(manifest_path, "r") as f:
            try:
                manifest = yaml.safe_load(f)
            except yaml.YAMLError as e:
                raise ValueError(f"Error while parsing {manifest_path} file: {e}")

        if args.verbose:
            print(f"=== Manifest file content ===\n{yaml.dump(manifest, indent=4)}")

        if args.verbose: print("   Done: Reading manifest file")

        yp = { **metadata, **manifest }

        yp['generation_date'] = datetime.now()
        yp['the_arquetype'] = args.package

        if args.verbose: print("Rendering archetype templates (generating code)")
        print(f'Processing archetype at: `{template_folder}`')
        for path, _, files in os.walk(template_folder):

            for file in files:
                print(f"Processing at path: `{path}', file: `{file}'")

                template_path = os.path.join(path,file)
                # Set template_folder as a path to find includes
                loader = FileSystemLoader([path, template_folder, common_folder] + other_common_dirs)
                environment = Environment(loader=loader, trim_blocks=True, lstrip_blocks=True)

                # Add custom filters
                environment.filters["unique_types"] = unique_types

                # Get template
                template = environment.get_template(file)

                cwd = os.getcwd()
                new_file_path = template_path.replace(template_folder, cwd)
                new_file_path = re.sub("/include/[^/]*?/", f"/include/{yp['project']['name']}/", new_file_path)
                new_file_path = new_file_path.replace(".jinja2.ifne", "")
                if '.jinja2_gen' in file:
                    new_file_path = new_file_path.replace(".jinja2_gen", "").replace(".", "_gen.")
                else:
                    new_file_path = new_file_path.replace(".jinja2", "")

                if 'jinja2.ifne' in file and os.path.exists(new_file_path):
                    print(f"skipping file with ifne since it already exists")
                    continue

                if args.verbose: print(f'rendering new file at: `{new_file_path}`')

                try:
                    open(new_file_path, mode="w", encoding="utf-8")
                except IOError:
                    os.makedirs(os.path.dirname(new_file_path))
                finally:
                    with open(new_file_path, mode="w", encoding="utf-8") as results:
                        results.write(template.render(yp))
                        if args.verbose: print(f"   Done: {new_file_path}")

        print("Generation completed")

        return 0
